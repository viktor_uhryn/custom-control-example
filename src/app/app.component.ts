import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  email = 'test@test.com';
  emailPattern = '^[^@]+@[^@]+\\.[^@]+$';

  submit(form: NgForm): void {
    console.log(form.value);
    console.log('VALID', form.valid);
  }
}
