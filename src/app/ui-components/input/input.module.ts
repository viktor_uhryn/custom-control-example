import { NgModule } from '@angular/core';
import { InputComponent } from './input';

@NgModule({
  declarations: [
    InputComponent
  ],
  exports: [
    InputComponent
  ]
})
export class InputModule {}
