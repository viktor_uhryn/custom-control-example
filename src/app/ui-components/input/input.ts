import { ChangeDetectionStrategy, Component, ElementRef, forwardRef, HostListener, Input, Optional, Self } from '@angular/core';
import { Control } from '../field/control';
import { NgControl } from '@angular/forms';
import { Subject } from 'rxjs';

@Component({
  selector: 'input[appInput]',
  template: '',
  styleUrls: ['./input.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [{
    provide: Control,
    useExisting: forwardRef(() => InputComponent)
  }]
})
export class InputComponent implements Control<string>{
  @Input()
  get value(): string {
    return this._ngControl ? this._ngControl.value : this._elementRef.nativeElement.value;
  }
  set value(value: string) {
    if (this._ngControl) {
      this._ngControl.control?.setValue(value);
    }
  }

  @Input()
  get required(): boolean { return this._required; }
  set required(value: boolean) {
    this._required = value;
    this.stateChanges.next();
  }
  private _required = false;

  get ngControl(): NgControl | null {
    return this._ngControl || null;
  }

  focused = false;

  stateChanges: Subject<void> = new Subject<void>();

  @HostListener('focus')
  focus(): void {
    this.focused = true;
    this.stateChanges.next();
  }

  @HostListener('blur')
  blur(): void {
    this.focused = false;
    this.stateChanges.next();
  }

  constructor(@Optional() @Self() private _ngControl: NgControl, private _elementRef: ElementRef) {}
}
