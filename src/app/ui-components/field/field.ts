import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ContentChild, ContentChildren, Optional, QueryList } from '@angular/core';
import { Control } from './control';
import { ErrorComponent } from './error';
import { NgForm } from '@angular/forms';

let uniqueId = 0;

@Component({
  selector: 'app-field',
  templateUrl: './field.html',
  styleUrls: ['./field.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FieldComponent {
  id = `app-field-${uniqueId++}`;

  required = false;

  @ContentChildren(ErrorComponent, {descendants: true})
  set errors(value: QueryList<ErrorComponent>) {
    this.hasErrors = value.length > 0;
  }
  hasErrors = false;

  get displayErrors(): boolean {
    return this._control != null &&
      this._control.ngControl != null &&
      this._control.ngControl.invalid === true &&
      this._ngForm != null && this._ngForm.submitted;
  }

  @ContentChild(Control, {static: false})
  get control(): Control<any> { return this._control; }
  set control(value: Control<any>) {
    this.required = value.required;
    this._control = value;
    value.stateChanges.subscribe(() => {
      this.required = this.control.required;
      this._changeDetectorRef.markForCheck();
    });
  }
  private _control!: Control<any>;

  constructor(private _changeDetectorRef: ChangeDetectorRef, @Optional() private _ngForm: NgForm) {
    if (_ngForm) {
      _ngForm.ngSubmit.subscribe(() => {
        _changeDetectorRef.markForCheck();
      });
    }
  }
}
