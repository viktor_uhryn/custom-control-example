import { NgModule } from '@angular/core';
import { FieldComponent } from './field';
import { LabelComponent } from './label';
import { HintComponent } from './hint';
import { ErrorComponent } from './error';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [
    FieldComponent,
    LabelComponent,
    HintComponent,
    ErrorComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    FieldComponent,
    LabelComponent,
    HintComponent,
    ErrorComponent
  ]
})
export class FieldModule {}
