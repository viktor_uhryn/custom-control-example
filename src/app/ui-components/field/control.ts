import { Observable } from 'rxjs';
import { NgControl } from '@angular/forms';

export abstract class Control<T> {
  value: T | null = null;
  abstract required: boolean;
  abstract stateChanges: Observable<void>;
  abstract ngControl: NgControl | null = null;
}
