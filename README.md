# CustomControlExample

TODO:

 - Create custom input component with (**<app-input>** vs **\<input appInput>**);
 - Think about **app-field** wrapper, which are gonna to handle things described before;
 - Add a label, a tooltip and an error;
 - Implement abstract class **Control**;
 - Implement **app-field**;
